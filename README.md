# Node.js [TypeScript] Server Template

This project is a template to bootstrap a Node.js app.

### Dependencies

-   [node.js] v14 or higher
-   [yarn] (compatible version with [node] >= v14.x)

## Development

To run app in your local development environment, inside the project
directory, you should;

1. Install dependencies

```shell
yarn
# npm install
```

2. Add Environment variables in `.env` file in the root directory of the project

```shell
touch .env
echo "APP_PORT=3000" > .env
```

3. Run app in local environment

```shell
yarn run dev
# npm run dev
```

### Usage

When you bootstrap the app on your local machine, it will be listening on the
specified port by you (let's say 3000). You can make `GET` requests to
`http://localhost:<APP_PORT>/book` to test template while running the app.

<!-- REFERENCES -->

[node.js]: https://nodejs.org/en/ 'Node.js'
[yarn]: https://yarnpkg.com/ 'Yarn'

### Environment Variables

-   `APP_PORT` (optional, default value is: 3000)
