import log from 'loglevel';

import './setup';
import { start } from './server';

const main = async () => {
    const env = process.env;
    const serverUrl = await start(env.APP_PORT);
    log.info(`Server is listening at: ${serverUrl}`);
};

main();
