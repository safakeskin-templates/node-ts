import log from 'loglevel';
import dotenv from 'dotenv';

log.setLevel('INFO');
dotenv.config();
