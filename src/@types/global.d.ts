declare namespace NodeJS {
    export interface ProcessEnv {
        // write down environment variables here to take advantage of TS
        APP_PORT?: number;
    }
}
