import express from 'express';
import { json, urlencoded } from 'body-parser';
import setup from './setup';

import bookRouter from './routes/book';

const app = express();
app.use(urlencoded({ extended: false }));
app.use(json());

app.use('/book', bookRouter);

export const start = async (port: number = 3000) => {
    await setup();
    return new Promise((resolve, reject) => {
        app.listen(port, () => resolve(`http://localhost:${port}`));
        app.on('error', reject);
    });
};

export default app;
