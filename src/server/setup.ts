import log from 'loglevel';

const setup = async () => {
    log.info(`Server setup can be handled here.`);
};

export default setup;
