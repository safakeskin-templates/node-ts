export interface BookResponse {
    id: string;
    name: string;
}
