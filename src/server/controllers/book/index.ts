import { Request, Response } from 'express';
import { controllerFlow } from '../../../util';
import { BookResponse } from '../../models/book';

const getAll = async (_req: Request, res: Response) => {
    res.send([{ id: '000000', name: 'Hello, World!' }] as BookResponse[]);
};

export default {
    getAll: controllerFlow(getAll)
};
