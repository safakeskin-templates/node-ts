import { Request, Response } from 'express';
import log from 'loglevel';
import { GenericError } from './types';

export const controllerFlow = <T = void>(
    cb: (req: Request, res: Response) => Promise<T>
) => {
    const handler = async (req: Request, res: Response) => {
        try {
            return await cb(req, res);
        } catch (e) {
            const error = e as GenericError;
            log.error(
                `Error occured while handling ${req.method} with originalUrl: ${
                    req.originalUrl
                }: ${JSON.stringify(error)}`
            );
            res.status(error.status || 500).send({ error: error?.error });
        }
    };
    return handler;
};
