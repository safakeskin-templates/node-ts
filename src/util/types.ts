export type GenericError = {
    status?: number;
    error?: string;
};
